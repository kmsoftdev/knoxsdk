package com.knoxsdk;

import android.app.admin.DevicePolicyManager;
import android.app.enterprise.ApplicationPolicy;
import android.app.enterprise.EnterpriseDeviceManager;
import android.app.enterprise.license.EnterpriseLicenseManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.sec.enterprise.AppIdentity;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    static final int DEVICE_ADMIN_ADD_RESULT_ENABLE = 1;
    private final static String ELM = "B25E5A8136D63616125D40EEE6A42D515D704D73F0A8B5DE584F8EE656FDAE3D8FB07F4301FF29738DED85FE5706513BCC249BAE3E9AAA86C0AA572D5C2DB7AC";
    //this static instance along with its getter
    // is idiomatic for two way binding between android and jni
    private static MainActivity activityForJni;

    //load native-lib, i.e. src/main/cpp/native-lib.cpp
    //this library is fully set up and linked in app/CMake.txt
    static {
        System.loadLibrary("native-lib");

    }

    private DevicePolicyManager mDevicePolicyManager;
    private EnterpriseDeviceManager mEnterpriseDeviceManager;
    private EnterpriseLicenseManager mEnterpriseLicenceManager;
    private ComponentName mDeviceAdmin;
    private ImageView mImageView;
    private Bitmap mBmpHolder;

    public static MainActivity activityForJni() {
        return activityForJni;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = (ImageView) findViewById(R.id.image);
        activityForJni = this;
        checkAdminPrivileges();
    }

    @Override
    protected void onDestroy() {
        activityForJni = null;
        super.onDestroy();
    }

    private void checkAdminPrivileges() {
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mDeviceAdmin = new ComponentName(MainActivity.this, MyDeviceAdminReceiver.class);
        if (mDevicePolicyManager.isAdminActive(mDeviceAdmin)) {
            checkLicence();
            ((Button) findViewById(R.id.oneShot)).setEnabled(true);
            ((Button) findViewById(R.id.continuousUpdates)).setEnabled(true);
        }
        else {
            obtainAdminPrivileges();
        }
    }

    public void obtainAdminPrivileges() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
        startActivityForResult(intent, DEVICE_ADMIN_ADD_RESULT_ENABLE);
    }

    private void checkRuntimePermissions() {
        mEnterpriseDeviceManager = new EnterpriseDeviceManager(this);
        ApplicationPolicy applicationPolicy = mEnterpriseDeviceManager.getApplicationPolicy();
        List<String> runtimePermissions = Arrays.asList("android.permission.sec.MDM_REMOTE_CONTROL");
        applicationPolicy.applyRuntimePermissions(new AppIdentity(getPackageName(), "any"), runtimePermissions, ApplicationPolicy.PERMISSION_POLICY_STATE_GRANT);
    }

    private void checkLicence() {
        mEnterpriseLicenceManager = EnterpriseLicenseManager.getInstance(this);
        mEnterpriseLicenceManager.activateLicense(ELM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DEVICE_ADMIN_ADD_RESULT_ENABLE) {
            checkLicence();
            ((Button) findViewById(R.id.oneShot)).setEnabled(true);
            ((Button) findViewById(R.id.continuousUpdates)).setEnabled(true);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void getOneShot(View view) {
        getOneShot();
    }

    /**
     * this native method is a link to native-lib module,
     * it executes Java_com_knoxsdk_MainActivity_getOneShot__
     */
    public native void getOneShot();

    /**
     * this method is executed by native-lib module when the screen bytes are obtained
     */
    public void screenUpdated(byte[] bytes) {
        if (bytes == null || bytes.length < 1) return;

//        Log.d("kmtag", "r: " + (bytes[0] & 0xFF));
//        Log.d("kmtag", "g: " + (bytes[1] & 0xFF));
//        Log.d("kmtag", "b: " + (bytes[2] & 0xFF));
//        Log.d("kmtag", "a: " + (bytes[3] & 0xFF));

        //due to an argument missmatch (knox for some reason returns RGBA_8888 when android standard
        // is ARGB_8888 every pixel has to have its bits shifted.
        int[] colors = new int[bytes.length / 4];

        for (int i = 0, j = 0; i < bytes.length; i += 4, j++) {
            colors[j] = Color.argb(bytes[i + 3] & 0xFF, bytes[i] & 0xFF, bytes[i + 1] & 0xFF, bytes[i + 2] & 0xFF);
        }

        mBmpHolder = Bitmap.createBitmap(colors, 0, 480, 480, 800, Bitmap.Config.ARGB_8888);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBmpHolder != null) {
                    mImageView.setImageBitmap(mBmpHolder);
                }
            }
        });

    }

    public void changeTestButtonCol(View view) {
        view.setBackgroundColor(new Random().nextInt());
    }

    /**
     * same as getOneShot but will continue to fetch
     * screen updates for as long as they are supplied by knox sdk
     */
    public void getContinuousUpdates(View view) {
        getContinuousUpdates();
    }

    public native void getContinuousUpdates();

    /**
     * close the session to stop updates
     */
    public void closeSession(View view) {
        closeSession();
    }

    public native void closeSession();
}
