#include <jni.h>
#include <string>
#include <android/log.h>
#include <sys/mman.h>
#include <RemoteDesktop.h>

android::IRemoteDesktop *remoteDesktop;
android::IRemoteDesktopListener *remoteDesktopListener;
unsigned char *mFrame;
JavaVM *javaVM = NULL;
jclass mainActivityClass;
bool *isFinishing;

//declared private helper methods
void closeSession();

void startSession(JNIEnv *env, jobject object);

//this listener kills itself after it captures one screen
class OneShotListener : public android::IRemoteDesktopListener {

public:
    virtual ~OneShotListener() {

    }

    virtual void screenChanged() {
        //old JNIEnv has been wiped by now, it cannot be cached (but the javaVM can)
        JNIEnv *thisThreadJniEnv;
        JavaVMAttachArgs args; //this can be NULL or args.version, args.name, args.group
        int result = javaVM->AttachCurrentThread(&thisThreadJniEnv, &args);

        //info to calculate buffer size etc.
        int hwWidth, hwHeight, mBytesPerPixel, pixelFormat;
        remoteDesktop->getScreenInfo(&hwWidth, &hwHeight, &mBytesPerPixel, &pixelFormat);
        android::PixelFormatDetail pixelFormatDetail;
        remoteDesktop->getScreenPixelFormatInfo(pixelFormatDetail);


        android::DirtyRegion dirtyRegion;


        dirtyRegion.maxRects = 100;

        if (!remoteDesktop->captureScreen(dirtyRegion)) {
            __android_log_print(ANDROID_LOG_DEBUG, "kmtag", "screen capture failed");
        };
        //only now does mFrame point to the area in memory where screen pixels are stored



        //prepare data
        jbyteArray screenBytes = thisThreadJniEnv->NewByteArray(
                hwWidth * hwHeight * mBytesPerPixel);
        thisThreadJniEnv->SetByteArrayRegion(screenBytes, 0,
                                             hwWidth * hwHeight * mBytesPerPixel,
                                             (jbyte *) (mFrame));


        //get calling activity (MainActivity->getActivityForJni());
        jmethodID activityForJniMethod = thisThreadJniEnv->GetStaticMethodID(mainActivityClass,
                                                                             "activityForJni",
                                                                             "()Lcom/knoxsdk/MainActivity;");
        jobject actualActivity = thisThreadJniEnv->CallStaticObjectMethod(
                mainActivityClass,
                activityForJniMethod);


        jmethodID updateScreenMethod = thisThreadJniEnv->GetMethodID(mainActivityClass,
                                                                     "screenUpdated", "([B)V");

        thisThreadJniEnv->CallVoidMethod(actualActivity, updateScreenMethod, screenBytes);

        //release memory
        thisThreadJniEnv->DeleteLocalRef(screenBytes);

        // unsubscribe from screen updates
        closeSession();
    }
};

//this listener captures screen indefinitely.
class ContinuousUpdatesListener : public android::IRemoteDesktopListener {

public:
    virtual ~ContinuousUpdatesListener() {

    }

    virtual void screenChanged() {
        JNIEnv *thisThreadJniEnv;
        JavaVMAttachArgs args; //this can be NULL or args.version, args.name, args.group
        int result = javaVM->AttachCurrentThread(&thisThreadJniEnv, &args);


        int hwWidth, hwHeight, mBytesPerPixel, pixelFormat;
        remoteDesktop->getScreenInfo(&hwWidth, &hwHeight, &mBytesPerPixel, &pixelFormat);
        android::PixelFormatDetail pixelFormatDetail;
        remoteDesktop->getScreenPixelFormatInfo(pixelFormatDetail);


        android::DirtyRegion dirtyRegion;


        dirtyRegion.maxRects = 100;

        if (!remoteDesktop->captureScreen(dirtyRegion)) {
            __android_log_print(ANDROID_LOG_DEBUG, "kmtag", "screen capture failed");
        };

        //now mFrame contains correct values



        jbyteArray screenBytes = thisThreadJniEnv->NewByteArray(

                hwWidth * hwHeight * mBytesPerPixel);
        thisThreadJniEnv->SetByteArrayRegion(screenBytes, 0,
                                             hwWidth * hwHeight * mBytesPerPixel,
                                             (jbyte *) (mFrame));


        jmethodID activityForJniMethod = thisThreadJniEnv->GetStaticMethodID(mainActivityClass,
                                                                             "activityForJni",
                                                                             "()Lcom/knoxsdk/MainActivity;");

        jobject actualActivity = thisThreadJniEnv->CallStaticObjectMethod(
                mainActivityClass,
                activityForJniMethod);

        jmethodID updateScreenMethod = thisThreadJniEnv->GetMethodID(mainActivityClass,
                                                                     "screenUpdated", "([B)V");

        thisThreadJniEnv->CallVoidMethod(actualActivity, updateScreenMethod, screenBytes);


        thisThreadJniEnv->DeleteLocalRef(screenBytes);

        if (isFinishing) {
            remoteDesktop->setListener(NULL);
            delete remoteDesktop;
        }
    }
};


extern "C"
void Java_com_knoxsdk_MainActivity_getOneShot__(
        JNIEnv *env,
        jobject obj /* this */) {

    startSession(env, obj);

    remoteDesktopListener = new OneShotListener();
    remoteDesktop->setListener(remoteDesktopListener);

}

void startSession(JNIEnv *env, jobject obj) {
    if (env->GetJavaVM(&javaVM) < 0) {
        __android_log_print(ANDROID_LOG_DEBUG, "kmtag", "failed to obtain javaVM");
    };
    jobject mainActivityLocalClass = env->GetObjectClass(obj);
    mainActivityClass = reinterpret_cast<jclass>(env->NewGlobalRef(mainActivityLocalClass));

    remoteDesktop = android::IRemoteDesktop::getInstance();
    int w = 480, h = 800, format = android::PF_RGBA_8888;
    int mFD;
    android::FDType mFDType;
    if (!remoteDesktop->
            setScreenInfo(w, h, format
    )) {
        __android_log_print(ANDROID_LOG_DEBUG,
                            "kmtag", "failed to set screen info");
    }

    if (!remoteDesktop->init()) {
        __android_log_print(ANDROID_LOG_DEBUG,
                            "kmtag", "failed to init session");
    };


    if (!remoteDesktop->getFrameBufInfo(&mFD, &mFDType)) {
        __android_log_print(ANDROID_LOG_DEBUG,
                            "kmtag", "failed to open framebuffer device");
    } else {
        __android_log_print(ANDROID_LOG_DEBUG,
                            "kmtag", "deviceId: %d");
    }


    if (mFDType == android::FD_DEV_FB0) {
//this really shouldn't happen, knox faq states this will fail on android 4.0+ so in this case presumably always

    } else if (mFDType == android::FD_SHARED_MEM) {
        mFrame = (unsigned char *) mmap(0, (size_t) w * h * 4, PROT_READ, MAP_SHARED, mFD, 0);
    } else {

    }
}

extern "C"
void Java_com_knoxsdk_MainActivity_getContinuousUpdates__(JNIEnv *env, jobject instance) {
    startSession(env, instance);
    remoteDesktopListener = new ContinuousUpdatesListener();
    remoteDesktop->setListener(remoteDesktopListener);

}

void closeSession() {
    remoteDesktop->setListener(NULL);
    delete remoteDesktop;
}


extern "C"
void Java_com_knoxsdk_MainActivity_closeSession__(
        JNIEnv *env,
        jobject object/* this */) {

    closeSession();


}
